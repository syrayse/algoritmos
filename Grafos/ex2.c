/*
Forma de utilizar se um grafo orientado é ciclico
é tentar calcular uma ordenação topolófica desse grafo.
Se tal for possível o grafo é acíclico
*/
#include <stdlib.h>

#define VMAX 20
#define NE   0

typedef struct vertice {
    int peso, dest;
    struct vertice *next;    
}*Vertice,*GraphL[VMAX];

typedef int GraphM[VMAX][VMAX];

//alinea (a)
//Kahn - retorna quantos elementos foram adicionados (Kahn Topological Sort)
int             KahnTSGraphL          (GraphL g, int seq[])           // Guarda em seq a ordenação.
    {
        int vAtual,inicio,fim,nAntecessores[VMAX],holder;
        Vertice it;

        //Inicializar array dos antecessores.
        //Percore todos os vertices theta(V).
        for(vAtual = 0; vAtual < VMAX; vAtual++)
            nAntecessores[vAtual] = 0;

        //Calcular os antecessores de cada um dos vertices, inicialmente.
        //Precisa de percorrer todos os vertices para incializar os antecessores theta(V+E).
        for(vAtual = 0; vAtual < VMAX; vAtual++)
            for(it=g[vAtual]; it; it=it->next)
                ++nAntecessores[vAtual->dest];
        
        //Inicializar "queue"
        inicio = fim = 0;

        //Colocar na queue todos os elementos que subscrevam à propriedade de não-antecessores
        //não é preciso ter uma queue dinamica ou idem aspas, pois no max, seq só tem VMAX de tamanho.
        //Precisa de percorrer todos os vertices, theta(V).
        for(vAtual = 0; vAtual < VMAX; vAtual++)
            if(nAntecessores[vAtual] == 0)          //Se não tem antecessores
                seq[fim++] = vAtual;                //Enqueue deste

        //Percore no máximo V+E, logo O(V+E)
        while (inicio < fim)
            {
                //Dequeue
                v = seq[inicio++];

                //Reduzir o antecessor de todos os vertices com origem em v.
                for(it=g[v]; it ; it=it->next)
                    {
                        --nAntecessores[it->dest];
                        //Se algum destes fica com n de antecessores a 0. fazer enqueue do mesmo.
                        if(nAntecessores[it->dest] == 0)
                            seq[fim++] = it->dest;
                    }
            }

        //Retornar exatamente quantos elementos estão na queue.
        //Logo os running time assintotico é de theta(V+E).
        return inicio;
    }

//1 se for aciclico, 0 se for ciclico
int             testeCycleGraphL    (GraphL g, int seq[])
    {
        return (KahnTSGraphL(g,seq)==VMAX);
    }

//Tarjan

//alinea (b)
//Escrito acima.

//alinea (c)
//kahn
int             KahnTSGraphM          (GraphM g, int seq[])           // Guarda em seq a ordenação.
    {
        int origin,dest,nAnt[VMAX],inicio,fim;
        
        for(dest = 0; dest < VMAX; dest++)
            nAnt[dest] = 0;

        for(dest = 0; dest < VMAX; dest++)
            for(origin = 0; origin < VMAX; origin++)
                if(g[origin][dest] != NE)
                    ++nAnt[dest];
        
        for(dest = 0; dest < VMAX; dest++)
            if(!nAnt[dest])
                seq[fim++] = dest;
        
        inicio = fim = 0;

        while(inicio < fim)
            {
                origin = seq[inicio++];
                for(dest = 0; dest < VMAX; dest++)
                    {
                        if(g[origin][dest] != NE)
                            --nAnt[dest];
                        if(nAnt[dest]==0)
                            seq[fim++] = dest;
                    }
            }
        
        return inicio;
   }

//1 se for aciclico, 0 se for ciclico
//Com matriz de adj o algoritmo corre em theta(V^2).
int             testeCycleGraphM    (GraphM g, int seq[])
    {
        return (KahnTSGraphM(g,seq)==VMAX);
    }

//tarjan