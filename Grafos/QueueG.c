#include <stdlib.h>
#include <stdio.h>

typedef int DATA;
typedef int (*fcompare) (const DATA d1, const DATA d2);

typedef struct queue {
    DATA *info;
    int size,init,used;
    fcompare f;
}*QUEUE;

int             compare_ints    (const int d1, const int d2)
    {
        return (d2-d1);
    }

QUEUE           make_queue       (int N, fcompare f)
    {
        QUEUE q = NULL;
        if(f)
            {
                q = (QUEUE)malloc(sizeof(struct queue));
                q->size = N; q->used = q->init = 0; q->f = f;
                q->info = (DATA*)malloc(sizeof(DATA)*q->size);
            }
        return q;
    }

int             double_queue     (QUEUE q)
    {
        int i,r = 0;
        DATA *newArray;

        if(q)
            {
                newArray = (DATA*)calloc(q->size*2,sizeof(DATA));

                if(!newArray)
                    return 1;

                for(i = 0; i < q->used; i++)
                    newArray[i+q->init] = q->info[(i+q->init)%(q->size)];

                q->size *=2;
                free(q->info);
                q->info = newArray;

            }
        else
            r = 1;
    }

void             enqueue     (QUEUE q, DATA d)
    {
        if(q->size==q->used)
            double_queue(q);
        
        q->info[(q->init+q->used)%(q->size)] = d;
        ++q->used;
    }

int              dequeue       (QUEUE q, DATA * d)
    {
        int r = 1;
        if(q->used>0)
            {
                *d = q->info[q->init];
                q->init = (q->init + 1)%(q->size);
                --q->used;
                printf("Just got %d out of jail, no bond!\n",*d);
            }
        else
            r = 0;
    }

int             search_queue    (QUEUE q, DATA d)
    {
        int i,r = 0;

        for (i = 0; i < q->used && !r; i++)
            if(!q->f(d,q->info[(i+q->init)%(q->size)]))
                r = 1;

        return r;
    }

void            free_queue     (QUEUE q)
    {
        free(q->info);
        free(q);
    }

void            list_queue      (QUEUE q)
    {
        int i;

        for (i = 0; i < q->used; i++)
            printf("%d:\t%d\n",i+1,q->info[(i+q->init)%q->size]);

        printf("start in %d, end in %d, with %d elements!\n,queue size of %d\n",q->init,(q->init+q->used)%q->size,q->used,q->size);
    }

int             is_empty        (QUEUE q)
    {
        return (q->used==0);
    }

// DEBUGGIN
int main ()
    {
        QUEUE q = make_queue(3,compare_ints);
        int v;
        enqueue(q,2);
        enqueue(q,3);
        dequeue(q,&v);
        enqueue(q,5);
        enqueue(q,6);
        enqueue(q,7);
        list_queue(q);

        free_queue(q);
    }