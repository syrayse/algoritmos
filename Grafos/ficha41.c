/*
Resolução da ficha 4.
Parte de representações.
*/
#include <stdlib.h>


//Numero maximo de vertices
#define     VMAX        10
//Identificativo de vertice nao existente
#define     NE          0

#define     min(a,b)    ((a<b)?a:b)

//Exercicio 1
//Definição de matriz de adj.
typedef struct vertice {
    int peso;
    int dest;
    struct vertice *next;
}*Vertice,*GraphL[VMAX];

//Definição de matriz de adj.
typedef int GraphM[VMAX][VMAX];

//Exercicio 2
//GraphL -> GraphM
void    convGraphLtoM   (GraphL src, GraphM dest)
    {
        int i,j;
        Vertice tmp;

        //Preenche tudo a zeros.
        for(i = 0; i < VMAX; i++)
            for(j = 0; j < VMAX; j++)
                dest[i][j] = NE;

        //Faz a transferencia de lista de adj para matrix.
        for(i = 0; i < VMAX; i++)
            for(tmp = src[i] ; tmp != NULL; tmp=tmp->next)
                dest[i][tmp->dest]=tmp->peso;

        //conversion done
    }

//GraphM -> GraphL
void    convGraphMtoL   (GraphM src, GraphL dest)
    {
        //Conversão feita de modo a que os vertices fiquem por ordem crescente
        int i,j;
        Vertice tmp;

        //init lista de adj
        for (i = 0; i < VMAX; i++)
            dest[i] = NULL;
        
        //Faz a transferencia de matrix para lista
        for (i = 0; i < VMAX; i++)
            for(j = VMAX - 1; j >= 0; j--)
                {
                    if(src[i][j] != NE)
                        {
                            tmp = (Vertice)malloc(sizeof(struct vertice));
                            tmp->peso = src[i][j];
                            tmp->dest = j;
                            tmp->next = dest[i];
                            dest[i] = tmp;
                        }
                }
    }

// Exercicio 3
// Grau de entrada e saida para matrizes de adj.
int     entradaGraphM   (GraphM g, int v)
    {
        int i,r = 0;
        for(i = 0; i < VMAX; i++)
            if(g[i][v] != NE)
                ++r;
        return r;
    }

int     saidaGraphM     (GraphM g, int v)
    {
        int i,r = 0;
        for(i = 0; i < VMAX; i++)
            if(g[v][i] != NE)
                ++r;
        return r;
    }

// Grau de entrada e saida para listas de adj.
int     entradaGraphL   (GraphL g, int v)
    {
        int i,r=0;
        Vertice tmp;

        for(i = 0; i < VMAX; i++)
            for(tmp = g[i]; tmp != NULL; tmp = tmp->next)
                if(tmp->dest==v)
                    ++r;
        
        return r;
    }

int     saidaGraphL     (GraphL g, int v)
    {
        int r;
        Vertice tmp;
        for(tmp = g[v], r=0; tmp != NULL; tmp =tmp->next,++r)
            ;
        return r;
    }

/*
Exercicio 4
Grau de entrada
    -matriz = O(V)
    -listas = O(V+E)

Grau de saida
    -matriz = O(v)
    -listas = O(v)
*/

//Exercicio 5
//matrix
int     weightEntradaGraphM     (GraphM g, int v)
    {
        int i,r=0;
        for(i=0; i < VMAX; i++)
            r+=(g[i][v]!=NE)?g[i][v]:0;
        return r;
    }

int     weightSaidaGraphM       (GraphM g, int v)
    {
        int i,r=0;
        for(i=0; i < VMAX; i++)
            r+=(g[v][i]!=NE)?g[v][i]:0;
        return r;
    }

int     capacidadeGraphM        (GraphM g, int v)
    {
        return(weightSaidaGraphM(g,v)-weightEntradaGraphM(g,v));
    }
//listas
int     weightEntradaGraphL     (GraphL g, int v)
    {
        int i,r=0;
        Vertice tmp;

        for(i = 0; i < VMAX; i++)
            for(tmp = g[i]; tmp != NULL; tmp=tmp->next)
                if(tmp->dest==v)
                    r+=tmp->peso;
        
        return r;
    }

int     weightSaidaGraphL       (GraphL g, int v)
    {
        int r=0;
        Vertice tmp;

        for(tmp = g[v]; tmp; tmp=tmp->next,r+=tmp->peso)
            ;
        
        return r;
    }

int     capacidadeGraphL        (GraphL g, int v)
    {
        return(weightSaidaGraphL(g,v)-weightEntradaGraphL(g,v));   
    }

//Exercicio 6
int     maximumVector           (int * v, int N)
    {
        int i,max=v[0];
        for(i=1; i < N; i++)
            max=(v[i]>max)?v[i]:max;
        return max;
    }

//O(V^2)
int     maxCapGraphM            (GraphM g)
    {
        int v[VMAX],i,j;

        for(i = 0; i < VMAX; i++)
            v[i] = 0;
        
        for(i = 0; i < VMAX; i++)
            for(j = 0; j < VMAX; j++)
                if(g[i][j]!=NE)
                    {
                        v[i]+=g[i][j];
                        v[j]-=g[i][j];
                    }
        
        return maximumVector(v,VMAX);
    }

//O(V+E)
int     maxCapGraphL            (GraphL g)
    {
        int v[VMAX],i;
        Vertice tmp;

        for(i = 0; i < VMAX; i++)
            v[i] = 0;

        for (i = 0; i < VMAX; i++)
            for (tmp = g[i]; tmp != NULL; tmp = tmp->next)
                {
                    v[i]+=tmp->peso;
                    v[tmp->dest]-=tmp->peso;
                }
        
        return maximumVector(v,VMAX);
    }

//Exercicio 7
int     colorOKGraphM           (GraphM g, int color[])
    {
        //Inicialmente assume que o grafo é válido
        int i,j,r=1;

        for(i = 0; i < VMAX && r; i++)
            for(j = 0; j < VMAX && r; j++)
                if(g[i][j]!=NE)
                    r = min(r,(color[i]!=color[j]));
        
        return r;
    }

int     colorOKGraphL           (GraphL g, int color[])
    {
        int i,r=1;
        Vertice tmp;

        for(i = 0; i < VMAX && r; i++)
            for(tmp = g[i]; tmp != NULL && r; tmp=tmp->next)
                r = min(r,(color[i]!=color[tmp->dest]));

        return r;
    }

//
void    inverseGraphM           (GraphM src, GraphM dest)
    {
        int i,j;

        for(i = 0; i < VMAX; i++)
            for(j = 0; j < VMAX; j++)
                dest[i][j] = NE;

        for(i = 0; i < VMAX; i++)
            for(j = 0; j < VMAX; j++)
                if(src[i][j]!=NE)
                    dest[j][i]=src[i][j];
    }

void    inverseGraphL           (GraphL src, GraphL dest)
    {
        int i;
        Vertice tmp,aux;

        for(i = 0; i < VMAX; i++)
            dest[i] = NULL;

        for(i = 0; i < VMAX; i++)
            for(tmp = src[i]; tmp != NULL; tmp = tmp->next)
                {
                    aux = (Vertice)malloc(sizeof(struct vertice));
                    aux->peso = tmp->peso;
                    aux->dest = i;
                    aux->next = dest[tmp->dest];
                    dest[tmp->dest] = aux;
                }
        
    }