/*
Exame de recurso de 2008/2009
função buildBST que dado um array de inteiros por ordem crescente
cria um arvore binaria de procura balanceada, com todos os elementos do array

*/

typedef struct node {
    int info;
    struct node *esq,*dir;
}*Node;

Node buildBST (int arr[], int N);
