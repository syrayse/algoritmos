/*
Considere AVL de inteiros
defina uma função que calcula a ltura de uma AVL em tempo logaritmico
no numero de nodos .
*/

#define     BAL     0   // balenced
#define     ESQ     -1  // esq mais pessada
#define     DIR     1   //dir mais pesada

typedef struct avlNode *AVL;
struct avlNode {
    int bal, valor;
    struct avlNode *esq,*dir;
};

int         altura      (AVL a)
    {
        int r = 0;

        if(a)
            {
                ++r;
                if(a->bal == ESQ)
                    r+=altura(a->esq);
                else
                    r+=altura(a->dir);
            }


        return r;
    }