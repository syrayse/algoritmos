/*
Considere min heaps de inteiros

(a)
Considere uma min heap de tamaho 100
e nas primeiras 10 posições contem os numeros
4,10,21,45,13,25,22,60,100,20

como irá ficar a min-heap depois da inserção do numero 6?
solução:
4,6,21,45,10,25,22,60,100,20,13

(b)
Defina uma funçã minHeapOk que testa se a min-heap está corretamente construida
É minheap se os filhos forem maiores que o pai
*/

typedef struct minheap {
    int size,used,*values;
}*MinHeap;

#define     pai(i)      ((i-1)/2)
#define     min(a,b)    ((a<b)?a:b)

int minHeapOK (MinHeap h)
    {
        int i,r = 1;
        for(i=1; i<h->used && r; i++)
            r = min(r,h->values[pai(i)]<h->values[i]);
        return r;
    }