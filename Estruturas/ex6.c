/*
Ex 6 - Exame de recurso 2008/09

estados sucessivos da AVL
por ordem 50,20,10,60,40,30,70

Step 0:
    * (Vazio)

Step 1:
        (50)^0
Step 2:
        (50)^1
        /        
     (20)^0
Step 3:
        (50)^2
        /
     (20)^1
     /
   (10)^0

   Não balanceada, reajuste:

        (20)^0
        /     \
     (10)^0   (50)^0

Step 4:

            (20)^-1
            /      \
       (10)^0      (50)^-1
                     \
                      (60)^0

Step 5:

            (20)^-1
           /       \
       (10)^0       (50)^0
                   /      \
                (40)^0    (60)^0

Step 6:

            (20)^-2
           /       \
       (10)^0       (50)^1
                   /      \
                (40)^1    (60)^0
                /
             (30)^0
    Está desbalanceada, procede-se a ajuste.

    Rotação à direita simples em (50)^1

            (20)^-2
           /       \
       (10)^0       (40)^-1
                   /      \
                (30)^0    (50)^-1
                             \
                             (60)^0

    Rotação à esquerda simples em (20)^-2

                (40)^0
               /      \
          (20)^0      (50)^-1
         /     \           \
     (10)^0    (30)^0      (60)^0

     Está balanceada!

Step 7:

                (40)^-1
               /      \
          (20)^0      (50)^-2
         /     \           \
     (10)^0    (30)^0      (60)^-1
                              \
                              (70)^0
    
    Rotação à equerda em (50)^-2

                  (40)^0
               /           \
          (20)^0            (60)^0
         /     \            /      \
     (10)^0    (30)^0   (50)^0    (70)^0
*/