/*
estrutura de dados para representar multi-conjuntos ( conjuntos com elementos repetidos).
de inteiros entre 0 e 1000, que suporte as seguinte operações
inserção, numero de ocorrencias, listagem ordenada

listagem linear
numero de ocorrencias constante

pode se utilizar direct addressing com um Arr de tamanho 1001.
para caberem todos os 1001 diferentes numeros.
*/
#include <stdlib.h>
#include <stdio.h>

#define         SIZE        1001

typedef int MultiConjunto[SIZE];

void    initMulCon      (MultiConjunto m)
    {
        int i;
        for (i = 0; i < SIZE; i++)
            m[i] = 0;
    }

void    inserOccur      (MultiConjunto m, int x)
    {
        m[x]++;
    }

int     checkOccur      (MultiConjunto m, int x)
    {
        return m[x];
    }

void    listOccur       (MultiConjunto m)
    {
        int i,p;
        for(i=0; i < SIZE; i++)
            for(p=m[i]; p>0; p--)
                printf("%d\n",m[i]);
    }