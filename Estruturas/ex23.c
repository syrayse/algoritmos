/*
Considere uma min-heap dinâmica de inteiros.
(a)
Numa destas está guardada uma min-heap com tamanho 100 com 10 elementos
as 10 primeiras posições possuem os seguintes valores:

4,10,21,45,13,25,22,60,100,20

Simule 2 remoções

Apos a primeira remoção:
10,13,21,45,20,25,22,60,100

Após a segunda remoção:
13,20,21,45,100,25,22,60

(b)
Considere a função que constroi uma arvore binaria
apartir de uma min-heap
analise a complexidade da função hToAux em função do tamanho da min-heap.


baseado no numero de comparações, a complexidade de hToAux corresponde à seguinte recurrencia.
n = 0
    tn = 1
tn = 1 + 2*(t(n/2))
tn = 1 + 2*(1+2*t(n/4)) = 3 + 4*t(n/4)
.
.
.
tn = O(N)



*/