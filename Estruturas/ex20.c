/*
Considere uma tabela de hash dinâmica com tratamento de colisões por chaining.

*/

#include <stdlib.h>

typedef struct entryC {
    char key[10];
    void *info;
    struct entryC *next;
}*EntryChain;
typedef struct hashT {
    int hashsize;
    EntryChain *table;
}*HashTableChain;

int hash(int size, char key[]);

// Fator de carga = (Nºde chaves)/(Tamanho do array) = Nºmédio de chaves por ranhura
float loadFactorC (HashTableChain t)
{
    int i,size = 0;
    EntryChain it;

    for(i=0; i<t->hashsize ; i++)
        for(it = t->table[i]; it != NULL; it=it->next,size++)
            ;
    
    return (size/((float)t->hashsize));
}
