/*
Estrutura de dados para representa conjuntos de inteiros
entre 0 e 1000

deve suportar
    inserção - constante
    teste de pertença - constante
    listagem ordenada no êcra - linear
*/
#include <stdio.h>
#include <stdlib.h>

#define     SIZE    1001        // entre 0 e 1000 há 1001 numeros
#define     EMPTY   -1          // casa vazia

typedef int Dict[SIZE];

void    initDict    (Dict d)
{
    int i;
    for (i = 0; i < SIZE ; i++)
        d[i] = EMPTY;
}

// 0 se adicionar, 1 se já existir
int     insertDict  (Dict d, int x)
{
    int r = 0;
    if(d[x] != EMPTY)
        r = 1;
    else
        d[x] = x;
    return r;
}

// 0 se estiver, 1 se não estiver
int     isInDict    (Dict d, int x)
{
    return (d[x] == EMPTY);
}

void    orderPrint  (Dict d)
{
    int i;
    for (i = 0; i < SIZE; i++)
        if(d[i] != EMPTY)
            printf("%d\n",d[i]);
}