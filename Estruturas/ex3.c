/*
3 - Exame de recurso de 2007/08

Analisar complexidade da função bals
função altura é linear.

void    bals    (Arvore a)
{
    if (!a) return;
    a->bal = altura(a->dir) - altura(a->esq);
    bals(a->esq);
    bals(a->dir);
}

Sabe-se que no máximo n é log2N
Resulta na seguinte recurrencia

se n <=0
Tn = 0;

se n>0;
Tn = n + 2*(T(n/2));                                iteração 1
Tn = n + 2*(n/2 + 2*(T(n/4))) = 2n + 4T(n/4);       iteração 2
Tn = 2n + 4*(n/4 + 2*(T(N/8))) = 3n + 8T(n/4);      iteração 3
...
Tn = N*log2N + 2^(log2N)*T(0);                      iteração log2N(max)
Tn = O(N*log2N);

*/