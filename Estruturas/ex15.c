/*
(a)
    Qual o pior caso do tempo de execução de uma pesquisa numa tabela de hash com n elementos
    inseridos, quando se utiliza chaining como mecanismo de resolução de colisões?

    O pior caso com chaining corresponde à situação em que todos os n elementos possuem a mesma
    imagem pela função de hash, logo todos os elementos "colidem".
    Pelo que a tabela de hash neste caso corresponde a uma lista contendo todos os elementos
    Logo o tempo de execução é O(N) - big-O LINEAR.

(b)
    Qual o tempo de execução de percorrer todas as entradas, por ordem dos seus valores,
    numa tabela de hash com n elementos, quando se utiliza chaining como mecanismo de resolução
    de colisões?

    Seja MAX o tamanho maximo da hash-table
    então para percorrer todos os elementos, deve se percorrer N elementos que surgirão, eventualmente
    e para além  disso percorrer MAX elementos de forma a verificar se há info nestes ou não
    logo T(N) = 0(N + MAX);

(c)
    Em que circunstâncias optaria pela utilização de uma tabela de hash?
    Quando a probabilidade de colisão fosse minima  de tal modo de houvesse a possibilidade MAX = N

*/