/*
Utilizar tabela de Hash com resolução de colisões por Chaining
em que o faxtor de carga atingiu os 50%

(a)
apresentar difinição de factor de carga e indique qual o valor máximo para este
parâmetro no caso do método de resolução apresentado

(b)
Qual o número de comparações no melhor caso de uma pesquisa falhada na tabela indicada? e no Pior caso?

resolução
(a)
Utilizando chaining o factor de carga que corresponde a (Nºde chaves)/(Tamanha do array) é só limitado
pela memória disponivél no computador, sendo que o Nº de chaves armazenadas não depende do tamanho de array
mas sim da capacidade do computador conseguir alocar memória para ranhuras correspondentes a novas chaves.

(b)
No melhor caso,
    Ao fazer lookup vai ser acedida uma ranhura do array que se encontra vazia. Pelo que, a pesquisa falhada
    é feita em tempo O(1).

No pior caso,
    A hashtable degenerou completamente em uma lista ligada. Ou seja todas as chaves estão contidas numa só
    ranhura do array. Pelo que uma pesquisa falhada, nesta posição irá ser linear, ou seja O(N).
    
*/