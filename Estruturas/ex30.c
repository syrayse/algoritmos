/*
Alternativa à avl que consiste em guardar, para cada nodo da arvore,
a altura da arvore que ai se inicia em vez de guardar o factor de balanco desse nodo

defina uma funçao AVL rotateLeft (AVL a) que faz um rotação (simples à esquerda) na raiz de uma desta arvores
assume-se que a rotação é sempre possivel, logo existe sempre o ramo direito.
*/
#include <stdlib.h>

#define     max(a,b)        ((a>b)?a:b)

typedef struct nodo{
    int valor, altura;
    struct nodo *esq,*dir;
}Node,*AVL;

int         heightAVL       (AVL a)
    {
        return (a?a->altura:0);
    }

AVL         rotateLeft      (AVL a)
    {
        AVL tmp = a->dir;
        a->dir = tmp->esq;
        a->altura = 1+max(heightAVL(a->esq),heightAVL(a->dir));
        tmp->esq = a;
        tmp->altura=1+max(heightAVL(tmp->esq),heightAVL(tmp->dir));
        return tmp;
    }