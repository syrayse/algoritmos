/*
duplica o tamanho da hash table
preservando a informação armazenada
a tabela é passada por referência e deve ser alterada
*/
#include <stdlib.h>

#define STATUS_FREE 0
#define STATUS_USED 1
#define STATUS_DELETED 2

typedef struct entryO {
    int status;
    char* key;
    void* info;
}EntryOAdd;

typedef struct hashT {
    int size;
    int used;
    EntryOAdd *table;
}*HashTableOAddr;

int         hash            (char* key, int size);

//Assumind que não ocorre o caso em que o espaço não é suficiente
//Pois estamos a duplicar o espaço mantendo o numero de elementos utilizados constante.
//Também assume que não há elementos apagados, pois inicialmente a nossa lista só contem frees
int         findProbe       (char* key, int size, EntryOAdd* arr)
    {
        int p = hash(key,size);
        while(arr[p].status != STATUS_FREE)
            p = (p+1)%size;
        return p;
    }

//Esta implementação só passa os valores usadas.
void        doubleTable     (HashTableOAddr h)
    {
        int p,i,nS = h->size*2;
        EntryOAdd *tmp,*nT = (EntryOAdd*)malloc(sizeof(struct entryO)*nS);

        //Set tudo a vazio inicialmente
        for(i = 0; i < nS; i++)
            nT[i].status = STATUS_FREE;
        
        //vai buscar os valores da tabela orginal(os usados) e faz rehash para a tabela atual.
        for(i=0; i < h->size; i++)
            if(h->table[i].status == STATUS_USED)
                {
                    p = findProbe(h->table[i].key,nS,nT);
                    nT[p].key = h->table[i].key;
                    nT[p].info = h->table[i].info;
                    nT[p].status = STATUS_USED;
                }
        // Todos os elementos foram transmitidos

        //Trocar a tabela
        tmp = h->table;
        h->table = nT;
        free(tmp);

        h->size *= 2;
    }