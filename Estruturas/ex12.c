/*
Teste 2008/09
considere o tipo ao lado para representar uma min-heap
defina um função que calcule o maior elemento da heap sem ter de percorrer necessariamente toda

OPCAO VIAVEL
Se eu tenho N elementos na minha heap
então eu sei que só apartir de (N-2)/2 é que
é possivel começar a observar elementos que possam ser folhas.

*/

#define maxH 1000
typedef struct mHeap {
    int tamanho;
    int heap [maxH];
} MinHeap;

int maxHeap (MinHeap m)
{
    int i, max = m.heap[m.tamanho-1];
    for(i = 1 + (m.tamanho-2)/2 ; i < m.tamanho - 1; i++)
        if(m.heap[i] > max)
            max = m.heap[i];
    return max;
}