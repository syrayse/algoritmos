/*
Evolução de uma arvore AVL
onde se inserem as seguintes chaves:

3 8 2 9 5 1 4 7 6

Insercao 0:
    *           (vazio)

Insercao 1:
    (3)^0

Insercao 2:
    (3)^-1
         \
         (8)^0

Insercao 3:
        (3)^0
       /     \
    (2)^0    (8)^0

Insercao 4:
        (3)^-1
       /     \
    (2)^0    (8)^-1
                 \
                 (9)^0

Insercao 5:
        (3)^-1
       /     \
    (2)^0    (8)^0
            /     \
          (5)^0   (9)^0

Insercao 6:
        (3)^0
       /     \
    (2)^1    (8)^0
   /         /     \
 (1)^0     (5)^0   (9)^0

Insercao 7:
        (3)^-1
       /     \
    (2)^1    (8)^1
   /         /     \
 (1)^0     (5)^1   (9)^0
           /
        (4)^0

Insercao 8:
        (3)^-1
       /     \
    (2)^1    (8)^1
   /         /     \
 (1)^0     (5)^0   (9)^0
         /    \
      (4)^0   (7)^0

Insercao 9:
        (3)^-2
       /     \
    (2)^1    (8)^2
   /         /     \
 (1)^0     (5)^-1   (9)^0
         /    \
      (4)^0   (7)^1
             /
          (6)^0

    Fica desbalanceada, pelo que se efetua uma rotação à direita centrada em  8

        (3)^-2
       /     \
    (2)^1     (5)^-2
   /         /     \
 (1)^0     (4)^0   (8)^1
                   /      \
                (7)^1   (9)^0
                 /
              (6)^0


    Fica desbalanceada, pelo que se efetua uma rotação à esquerda centrada em  8

                    (5)^0
                   /      \
              (3)^1        (8)^1
            /     \       /       \
        (2)^1   (4)^0   (7)^1       (9)^0
        /              /              
     (1)^0            (6)^0
*/