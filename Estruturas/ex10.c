/*
teste 2008/09
implementando AVL's

função que dados 2 inteiros verifica se há na AVL um numero intermedio.
*/

typedef struct nodo *ABPInt;
struct nodo {
    int valor;
    ABPInt esq,dir;
}

#define max(a,b) ((a>b?a:b))

int procura (ABPInt a, int l, int u)
{
    int r = 0;                          // Não existe em (l,u)
    while(a)
    {
        r = (a->valor>l && a->valor<u);
        if(!r)
        {
            if(l > a->valor)
                a = a->esq;
            else if (u < a->valor)
                a = a->dir;
        }
    }
    return r;
}