/*
teste 2007/08

função que converte min-heap
representada em array para um min-heap 
representada como uma arvore do tipo
*/
#include <stdlib.h>
#define TAM 1000

#define PAI(i) ((i-1)/2)
#define ESQ(i) ((2*i + 1))
#define DIR(i) ((2*i + 2))

typedef int Heap[TAM];

typedef struct nodo {
    int val;
    struct nodo *esq,*dir;
} Nodo,*Tree;

Tree    buildTree   (Heap h, int nOrder)
{
    Tree r = NULL;
    if(nOrder < TAM)
    {
        r = (Tree)malloc(sizeof(struct nodo));
        r->val = h[nOrder];
        r->esq = buildTree(h,ESQ(nOrder));
        r->dir = buildTree(h,DIR(nOrder));
    }
    return r;
}

Tree    arrToTree   (Heap h)
{
    return buildTree(h,0);
}