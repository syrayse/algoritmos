/*
teste 2007/08
implementar dicionário de sinónimos usando uma
tabela de hash. Esta tabela pode ser definida
da seguinte forma, onde a cada palavra está
associada uma lista ligada com os seus sinónimos

Ou seja, a entrada p = hash(x_word) da hash table
se possuir x_word, então deve se imprimir Syn.
utilizando chaining/closed adressing.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct s
{
    char *syn;
    struct s *next;
}Syn;

typedef struct p
{
    char *pal;
    Syn *sins;
    struct p *next;
}Pal;

#define     TAM     1000
typedef Pal *Dic[TAM];

int     hash        (char *pal);

void    sinonimos    (Dic d, char *pal)
{
    int p=hash(pal);
    Pal *aux;
    Syn *s;
    for(aux = d[p]; aux; aux=aux->next)
        if(!strcmp(aux->pal,pal))
            for(s=aux->sins; s ; s=s->next)
                printf("%s\n",s->syn);
}

int main()
{

}
