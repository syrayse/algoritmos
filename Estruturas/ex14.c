/*
Represente graficamente a evolução de um arvore AVl
quando é efetuada a seguinte sequência de inserções
10 20 30 70 40 50
!!indica fator de balanceamento.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Insercao 0:
    *           (vazio)

Insercao 1:
    (10)^0

Insercao 2:
    (10)^-1
         \
         (20)^0

Insercao 3:
    (10)^-2
          \
          (20)^-1
               \
               (30)^0
    
    Fica desbalanceada! logo efetua-se uma rotação simples à esquerda
    
                (20)^0
               /      \
           (10)^0     (30)^0

Insercao 4:

                (20)^-1
               /      \
           (10)^0     (30)^-1
                          \
                          (70)^0

Insercao 5:

                (20)^-2
               /      \
           (10)^0     (30)^-2
                          \
                          (70)^1
                          /
                       (40)^0
       Arvore fica desbalanceada, faz-se rotação simples à direita em (70)^1

                (20)^-2
               /      \
           (10)^0     (30)^-2
                          \
                          (40)^-1
                             \
                              (70)^0
        Continua desbalanceada, efetua-se rotação simples à esquerda em (30)^-2

                (20)^-1
               /      \
           (10)^0     (40)^0
                     /       \
                (30)^0      (70)^0

Insercao 6:

                (20)^-2
               /      \
           (10)^0     (40)^-1
                     /       \
                (30)^0      (70)^1
                           /
                      (50)^0
        Fica desbalanceada, efetua-se uma rotação à esquerda simples, centrada em (20)^-2

                    (40)^0
                  /       \
            (20)^0         (70)^1
           /      \        /
       (10)^0   (30)^0   (50)^0
       DONE!!
*/