/*
Exercicios 1 (Exame de recurso 2007/08)
Usar tabela de HASH estática com closed adressing.
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define     SIZE    1009
typedef struct no
{
    char matricula[6];
    struct no *next;
} No;
typedef No *Tabela[SIZE];

void    initTabela  (Tabela t)
{
    int i;
    for (i = 0; i < SIZE; i++)
        t[i] = NULL;
}

int     hash        (char matricula[6])
{
    int i,r=0;
    for (i = 0; i < 3; i++)
        r+=matricula[i];
    for (   ; i < 6; i++)
        r*=matricula[i];
    return (r%SIZE);
}

int     insert      (Tabela t, char matricula[6])
{
    int r,p = hash(matricula);  
    No **aux,*tmp;                            // Usado para prosseguir ao longo do valor de hash
    r = 0;                               // Inicialmente não há repetidos

    for (aux=t+p; (*aux) != NULL && !r; aux=&((*aux)->next))
        if (!strcmp((*aux)->matricula,matricula))       // Encontra repetido
            r = 1;

    if(!r)                                              // Se não houver repetido
    {
        tmp = (No*)malloc(sizeof(struct no));
        strcpy(tmp->matricula,matricula);
        tmp->next = *aux;
        *aux = tmp;
    }

    return r;
}

void    printTabela (Tabela t)
{
    int i,r=0;;
    No *aux;
    for (i = 0; i < SIZE; i++)
        for(aux = t[i]; aux ; aux=aux->next)
        {
            ++r;
            printf("N%d\tp:%d\t%s\n",r,i,aux->matricula);   
        }
}

// Debugging
int     main        (void)
{
    Tabela t;
    initTabela(t);

    insert(t,"XOEXEF");
    insert(t,"XO24EF");
    insert(t,"XO1XEF");
    insert(t,"2OEXEF");

    printTabela(t);
}