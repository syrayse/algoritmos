/*
considere um tabela de hash para implementar um conjunto de inteiros com tratamento
de colisões por open addressing (com linear probing) e em que a remoção das chaves é feita usando
uma marca de apagado.

Suponha que existem defnidas as funções add (int k), remove (int k) e exists(int k), esta ultima devolve
verdadeiro ou falso.

suponha que o tamanho da tabela é 7 e a função é hash(x) = x % 7.

apresenta a evolução da tebal, quando apartir de uma tabela vazia se aplica a seguinte sequencia.

X   -> USED_SPACE
F   -> FREE_SPACE
R   -> REMOVED_SPACE

iteração 0:

|----|----|----|----|----|----|----|
|  F |  F |  F |  F |  F |  F |  F |
|----|----|----|----|----|----|----|

iteração 1:
add 15 => hash(15) = 1

|----|----|----|----|----|----|----|
|  F |15X |  F |  F |  F |  F |  F |
|----|----|----|----|----|----|----|

iteração 2:
add 25 => hash(25) = 4

|----|----|----|----|----|----|----|
|  F |15X |  F |  F |25X |  F |  F |
|----|----|----|----|----|----|----|

iteração 3:
add 9 => hash(9) = 2

|----|----|----|----|----|----|----|
|  F |15X | 9X |  F |25X |  F |  F |
|----|----|----|----|----|----|----|

iteração 4:
add 1 => hash(1) = 1
COLISÃO!! celula 1 já está ocupada
probing levá até a celula 3

|----|----|----|----|----|----|----|
|  F |15X | 9X | 1X |25X |  F |  F |
|----|----|----|----|----|----|----|

iteração 5:
rem 9 => hash(9) = 2

|----|----|----|----|----|----|----|
|  F |15X |  R | 1X |25X |  F |  F |
|----|----|----|----|----|----|----|

iteração 6:
add 38 => hash(38) = 3
COLISAO!! celula 3 está ocupada, leva até celula 5

|----|----|----|----|----|----|----|
|  F |15X |  R | 1X |25X |38X |  F |
|----|----|----|----|----|----|----|

iteração 7:
rem 15 => hash(15) = 1

|----|----|----|----|----|----|----|
|  F |  R |  R | 1X |25X |38X |  F |
|----|----|----|----|----|----|----|

iteração 8:
add 6 => hash(6) = 6

|----|----|----|----|----|----|----|
|  F |  R |  R | 1X |25X |38X | 6X |
|----|----|----|----|----|----|----|

iteração 9:
add 10 => hash(10) = 3
COLISAO!! celula 3 está ocupada leva até a celula 0

|----|----|----|----|----|----|----|
|10x |  R |  R | 1X |25X |38X | 6X |
|----|----|----|----|----|----|----|

*/