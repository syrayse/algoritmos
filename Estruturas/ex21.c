
/*
Assume-se que palavras são sequencias de letras do alfabeto
a cada um atribuida de uma significancia por int rank[26].
quanto mais frequente a letra menor é o seu rank.

*/

#include <stdlib.h>

// rank é global??
int rank[26];

char mytolower (char c)
{
    if (c>='A' && c<='Z')
        c+='a'- 'A';
    return c;
}

int     hash    (int size, char * s)
{
    int i,r=0;
    for (i = 0; s[i] != '\0'; i++)
        r+=rank[mytolower(s[i])-'a'];
    return (r%size);
}