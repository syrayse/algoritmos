/*
Implementar um função que efetue uma rotação à direita simples
deve retornar:
0 em caso de sucesso
-1 no caso de rotação impossivel.
Não necessita de ajustar os factos de balanceamento.
*/

#include <stdlib.h>

typedef struct avlnode {
    int value, balance;
    struct avlnode *esq,*dir;
}*AVL;

int rdir (AVL *tptr)                  // Passa o endereço de memoria da propria AVL tree
{
    int r = 0;                          //Assume inicialmente ser possivel efetuar a rotacao
    AVL tmp;
    /*
    É impossivél realizar uma rotação à direita simples só se a estrutura da arvore for tal
    que não exista o ramo da esquerda ou a arvore seja nula. sendo que não faz sentido efetuar rotações
    nessas situações
    */
   if (*tptr == NULL || (*tptr)->esq == NULL)
        r = -1;

    if(!r)
    {
        tmp = (*tptr)->esq;
        (*tptr)->esq = tmp->dir;
        tmp->dir = *tptr;
        *tptr = tmp;
    }
    return r;
}