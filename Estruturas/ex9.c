/*
Teste - 2008/09

min-heap
definir função muda que altera o valor do
elemento que está na posição pos para valor
matendo as propriedades da heap
*/

#include <stdlib.h>

#define MaxH 1000

#define pai(i)  ((i-1)/2)
#define esq(i)  ((2*i+1))
#define dir(i)  ((2*i+2))

typedef struct mHeap {
    int tamanho;
    int heap[MaxH];
} *MinHeap;

void    swap    (int * arr, int i, int j)
{
    int tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
}

// 0 se conseguir adicionar, 1 caso contrario
int     muda    (MinHeap h, int pos, int valor)
{
    int c,r,tmp;
    r = 0;
    if (pos < h->tamanho)
        r = 1;
    if (!r)
    {
        //Muda o valor , quebra propriedade de heap
        h->heap[pos] = valor;

        //Verifica se é maximo entre o que está atrás e corrige
        tmp = pos;
        while(tmp > 0 && pai(tmp) > h->heap[tmp])
        {
            swap(h->heap,pai(tmp),tmp);
            tmp = pai(tmp);
        }

        // Verifica se há minimo entre o que está à frente
        if (tmp == pos)
        {
            while (esq(tmp) < h->tamanho)
            {
                tmp = esq(tmp);
                if (tmp + 1 < h->tamanho && h->heap[tmp + 1]<h->heap[tmp])
                    ++tmp;
                
                if(h->heap[pos]<h->heap[tmp]) break;
                swap(h->heap,pos,tmp);
                pos = tmp;
            }
        }
    }
    return r;
}