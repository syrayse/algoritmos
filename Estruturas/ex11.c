/*
Melhor caso:
    Elemento na raiz está contido em (l...u), logo o função é constante no melhor caso

Pior caso:
    Nenhum elemento da arvore está no intervalo (l..u) sendo que algoritmo tenta filtrar as possibilidades
    resultando na seguinte recurrencia com base em comparações

    n = 0
        tn = 0
    n != 0
        tn = 1 + t(n/2);
        Como se máximo só é possivel dividir n por log2n, então
        no pior tn = O(log2n).
*/