/*
Tabela de hash dinamicas com tratamento de colisões por chaining.

fazer uma função que inicializa hashtable.
*/

#include <stdlib.h>

typedef struct entry {
    char key[10];
    void *info;
    struct entry *next;
}*Entry;
typedef struct hashT {
    int hashsize;
    Entry *table;
}*HashTable;

HashTable           newTable        (int hashsize)
    {
        int i;
        HashTable r = (HashTable)malloc(sizeof(struct hashT));
        r->hashsize=hashsize;
        r->table=(Entry*)malloc(sizeof(Entry)*hashsize);
        for(i=0; i < hashsize; i++)
            r->table[i] = NULL;
        return r;
    }