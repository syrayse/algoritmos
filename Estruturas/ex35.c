/*
Considere o seguinte tipo de dados para armazenar um conjunto de pares - string
com um máximo de 10 caracteres e inteiro, utilizando uma arvore AVL ordenada pela string.
(string[10],inteiro)


*/
typedef struct data {
    int dados;
}Data;
typedef struct node {
    int balance;
    char key[11];
    Data info;
    struct node *left,*right;
}Node;
typedef Nodo *Dictionary;