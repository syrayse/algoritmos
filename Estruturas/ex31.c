/*
Tabela de hash dinamicas com tratamento de colisoes por open adressing e linear probing.
assumese a existencia de uma função int hash/key,int)

definir função que remove os elementos apagados de uma tabela, i.e.,
que efetua as operações necessarias para que deixem de existir celulas marcadas como apagadas
*/
#include <stdlib.h>

#define     LIVRE       0
#define     OCUPADO     1
#define     APAGADO     2

typedef struct key *Key;
struct celula {
    Key k;
    void *info;
    int estado;
};

typedef struct shash {
    int tamanho, ocupados, apagados;
    struct celula *Tabela;
}*THash;

int hash (Key k, int size);

int         findProbe       (struct celula *T, Key k, int size)
    {
        int p = hash(k,size);
        while (T[p].estado != LIVRE)
            p = (p+1)%size;
        return p;
    }

void        remApagados     (THash h)
    {
        int p,i;
        struct celula *tmp,*T = (struct celula*)malloc(sizeof(struct celula)*h->tamanho);
        for (i=0; i < h->tamanho; i++)
            T[i].estado = LIVRE;
        
        for (i=0; i < h->tamanho; i++)
            {
                p = findProbe(T,h->Tabela[i].k,h->tamanho);
                T[p].k = h->Tabela[i].k;
                T[p].info = h->Tabela[i].info;
                T[p].estado = OCUPADO;
            }
        tmp = h->Tabela;
        h->Tabela = T;
        free(tmp);
    }