/*
Considerar uma min-heap dinamica de inteiros

definir função int *ordenados (MinHeap h)
que calcula um array ordenado com os elementos da min-heap
assuma que existe uma funçao void bubbleDown (MinHeap h)

A função deve alocar o espaço do array resultado e destruir o conteudo da MinHeap
*/

#include <stdlib.h>


typedef struct minheap {
    int size, used;
    int *values;
}*MinHeap;

//Assuma que existe esta função
void    bubbleDown  (MinHeap h);

int*    ordenados   (MinHeap h)
{
    // Assume que minHeap é vazia 
    int added,N,*r = NULL;

    // Se de factor existe heap, alocar só o espaço necessário para o array
    if(h != NULL)
    {
        r = (int*)malloc(sizeof(int)*h->used);
        
        //Colocar todos os elementos no array por ordem
        for(added=0,N=h->used; added < N; added++)
        {
            r[added] = h->values[0];
            
            // Quebra propriedade de heap
            h->values[0] = h->values[--h->used];

            //Reajusta heap
            bubbleDown(h);
        }

        //liberta a heap
        free(h->values);
        free(h);
    }

    return r;
}